ics-build-elog
==============

**WARNING: this repository is deprecated!**

Please check https://gitlab.esss.lu.se/ics-software/elog

Repository to build ELOG_ with Kerberos support.


Usage
-----

The RPM is built automatically and uploaded to artifactory by Jenkins
(you can check the Jenkinsfile).

To build it locally run::

    $ docker build -t build-elog .
    $ docker run --rm -v $(pwd)/RPMS:/root/rpmbuild/RPMS build-elog


.. _ELOG: http://midas.psi.ch/elog/

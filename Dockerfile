FROM centos:7

RUN yum update -y && \
    yum install -y \
      gcc \
      git \
      make \
      which \
      openssl-devel \
      rpm-build && \
    yum clean all && \
    rm -rf /var/cache/yum

# Prepare the environment for building rpms and download source
RUN mkdir -p /root/rpmbuild/{BUILD,RPMS,SOURCES,SPECS,SRPMS} && \
    curl -o /root/rpmbuild/SOURCES/elog-3.1.3-1.tar.gz http://midas.psi.ch/elog/download/tar/elog-3.1.3-1.tar.gz

COPY enable-kerberos.patch /root/rpmbuild/SOURCES/

WORKDIR /build

COPY elog.spec /build/

CMD ["rpmbuild", "-bb", "elog.spec"]
